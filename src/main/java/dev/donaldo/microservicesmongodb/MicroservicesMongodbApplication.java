package dev.donaldo.microservicesmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicesMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicesMongodbApplication.class, args);
	}

}
