package dev.donaldo.microservicesmongodb.repository;

import dev.donaldo.microservicesmongodb.entity.ProductEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepositoy extends MongoRepository<ProductEntity, String> {
}
